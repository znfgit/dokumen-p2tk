/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     6/7/2014 3:52:32 PM                          */
/*==============================================================*/


drop index PENGGUNA_PK;

drop table PENGGUNA;

drop index PENGGUNA_TICKET_FK;

drop table TICKET;

/*==============================================================*/
/* Table: PENGGUNA                                              */
/*==============================================================*/
create table PENGGUNA (
   PENGGUNA_ID          INT8                 not null,
   NAMA                 VARCHAR(50)          null,
   constraint PK_PENGGUNA primary key (PENGGUNA_ID)
);

/*==============================================================*/
/* Index: PENGGUNA_PK                                           */
/*==============================================================*/
create unique index PENGGUNA_PK on PENGGUNA (
PENGGUNA_ID
);

/*==============================================================*/
/* Table: TICKET                                                */
/*==============================================================*/
create table TICKET (
   PENGGUNA_ID          INT8                 not null,
   TICKET_ID            INT8                 not null,
   TITLE                VARCHAR(100)         null,
   DESCRIPTION          VARCHAR(200)         null
);

/*==============================================================*/
/* Index: PENGGUNA_TICKET_FK                                    */
/*==============================================================*/
create  index PENGGUNA_TICKET_FK on TICKET (
PENGGUNA_ID
);

alter table TICKET
   add constraint FK_TICKET_PENGGUNA__PENGGUNA foreign key (PENGGUNA_ID)
      references PENGGUNA (PENGGUNA_ID)
      on delete restrict on update restrict;

