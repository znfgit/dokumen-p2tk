/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     6/7/2014 7:09:05 PM                          */
/*==============================================================*/


drop table if exists PENGGUNA;

drop table if exists TICKET;

/*==============================================================*/
/* Table: PENGGUNA                                              */
/*==============================================================*/
create table PENGGUNA
(
   PENGGUNA_ID          bigint not null,
   NAMA                 varchar(50),
   primary key (PENGGUNA_ID)
);

/*==============================================================*/
/* Table: TICKET                                                */
/*==============================================================*/
create table TICKET
(
   TICKET_ID            bigint not null,
   PENGGUNA_ID          bigint not null,
   TITLE                varchar(100),
   DESCRIPTION          varchar(200),
   primary key (TICKET_ID)
);

alter table TICKET add constraint FK_PENGGUNA_TICKET foreign key (PENGGUNA_ID)
      references PENGGUNA (PENGGUNA_ID) on delete restrict on update restrict;

