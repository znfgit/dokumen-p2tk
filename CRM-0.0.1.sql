SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `crm_p2tk_dikdas` ;
CREATE SCHEMA IF NOT EXISTS `crm_p2tk_dikdas` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `crm_p2tk_dikdas` ;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`Pengguna`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`Pengguna` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`Pengguna` (
  `pengguna_id` INT NOT NULL AUTO_INCREMENT ,
  `nama` VARCHAR(255) NOT NULL ,
  `level` CHAR(1) NULL ,
  `created_at` TIMESTAMP NOT NULL DEFAULT now() ,
  `last_login` TIMESTAMP NOT NULL DEFAULT now() ,
  `soft_delete` CHAR(1) NOT NULL ,
  PRIMARY KEY (`pengguna_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`Post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`Post` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`Post` (
  `post_id` INT NOT NULL AUTO_INCREMENT ,
  `title` TEXT NOT NULL ,
  `pengguna_id` INT NOT NULL ,
  `soft_delete` CHAR(1) NOT NULL ,
  `created_at` TIMESTAMP NOT NULL DEFAULT now() ,
  `post_history_id` INT NULL ,
  PRIMARY KEY (`post_id`) ,
  INDEX `fk_Post_Pengguna1_idx` (`pengguna_id` ASC) ,
  CONSTRAINT `fk_Post_Pengguna1`
    FOREIGN KEY (`pengguna_id` )
    REFERENCES `crm_p2tk_dikdas`.`Pengguna` (`pengguna_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`Ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`Ticket` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`Ticket` (
  `ticket_id` INT NOT NULL AUTO_INCREMENT ,
  `status` CHAR(1) NOT NULL ,
  `title` TEXT NOT NULL ,
  `pengguna_id` INT NOT NULL ,
  `post_id` INT NOT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT now() ,
  `soft_delete` CHAR(1) NULL ,
  PRIMARY KEY (`ticket_id`) ,
  INDEX `fk_Ticket_Pengguna1_idx` (`pengguna_id` ASC) ,
  INDEX `fk_Ticket_Post_idx` (`post_id` ASC) ,
  CONSTRAINT `fk_Ticket_Pengguna1`
    FOREIGN KEY (`pengguna_id` )
    REFERENCES `crm_p2tk_dikdas`.`Pengguna` (`pengguna_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ticket_Post`
    FOREIGN KEY (`post_id` )
    REFERENCES `crm_p2tk_dikdas`.`Post` (`post_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`Log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`Log` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`Log` (
  `log_id` INT NOT NULL AUTO_INCREMENT ,
  `pengguna_id` INT NOT NULL ,
  `message` TEXT NOT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT now() ,
  PRIMARY KEY (`log_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`Post_History`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`Post_History` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`Post_History` (
  `post_history_id` INT NOT NULL AUTO_INCREMENT ,
  `post_id` INT NOT NULL ,
  `to_pengguna_id` INT NOT NULL ,
  `from_pengguna_id` INT NOT NULL ,
  `created_at` TIMESTAMP NOT NULL DEFAULT now() ,
  `pesan` TEXT NOT NULL ,
  `soft_delete` CHAR(1) NOT NULL ,
  `post_status` INT NOT NULL ,
  PRIMARY KEY (`post_history_id`) ,
  CONSTRAINT `fk_post_history_post`
    FOREIGN KEY (`post_history_id` )
    REFERENCES `crm_p2tk_dikdas`.`Post` (`post_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `crm_p2tk_dikdas`.`post_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm_p2tk_dikdas`.`post_status` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `crm_p2tk_dikdas`.`post_status` (
  `post_status_id` INT NOT NULL ,
  `text` VARCHAR(45) NULL ,
  PRIMARY KEY (`post_status_id`) )
ENGINE = InnoDB;

SHOW WARNINGS;
USE `crm_p2tk_dikdas` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
