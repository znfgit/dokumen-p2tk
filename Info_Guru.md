Dokumentasi Info Guru
=====================

##Tentang Info Guru yang sudah ada
Seperti yang kita ketahui, Aplikasi info guru yang sudah berjalan saat ini menggunakan resource yang besar, yaitu `4 web server Apache` dan `1 web server IIS`. Traffic yang diterima oleh server sangat banyak sekali, bisa dilihat melalui counter `3.467.106` kali diakses (`07-10-2013`).

Dan banyak kendala, seperti server down, karena tidak sanggup menangani beban traffic yang sangat tinggi, juga database server tidak kuat dalam memproses `request` yang sangat banyak. Pada saat jarang yang mengakses, untuk memunculkan seorang data ptk, dibutuhkan waktu `1.40 detik`, dan ini pun bisa menjadi lebih besar, terlebih saat traffic tinggi, bahkan yang tertinggi pernah tercatat yaitu `9 detik`, hingga server down, dan tidak dapat diakses.

Maka, diperlukan lah maintenance untuk menangani masalah tersebut.

##Mengapa harus maintenance
Maintenance diperlukan supaya ptk - ptk yang ingin mengetahui info, dapat langsung mengakses dengan cepat, dan tidak perlu antri untuk mendapatkan info, atau bahkan tidak perlu menunggu lama karena server down.

##Planning maintenance
Maintenance ini bertujuan untuk mendapatkan hasil yang sama seperti `info guru yang sudah ada`, dan dengan tambahan akses yang cepat, juga dapat diakses melalui mobile browser dengan tampilan yang elegan.

###1. Database Server
Database yang dipilih jelas - jelas bukan `rdbms`, karena menggunakan `SQL SERVER` yang notabene database yang hebat, kuat (dan mahal), seharusnya mampu menangani `request` yang sangat banyak.

Namun teknologi tetap berlanjut, saat ini yang sedang berkembang adalah teknologi `nosql`. Secara umum `nosql` memilik arti `Not Only SQL (Structured Query Language)`. Dan, betul `nosql` ini hampir tidak memiliki struktur, dan itulah yang membuat `nosql` ini lebih cepat, atau dalam bahasa umumnya `schemaless`.

Teknologi `nosql` yang akan Kami pergunakan adalah [`redis`](http://redis.io), yaitu sebuah nosql yang menggunakan `Memory / RAM` sebagai penyimpanan utama. Menurut bahasa developer `redis` sendiri memiliki istilah `memcached with steroid`

###2. Web Server
Web server yang dipilih masih menggunakan `Apache` dan versi yang Kami gunakan adalah `Apache 2.2.22`. Apache hingga saat ini masih mencukupi request yang berdatangan, dan belum pernah down. Namun, jika `Apache` tidak mumpuni, maka `nginx` adalah pilihan terbaik untuk aplikasi web seperti ini.

###3. Bahasa Pemrograman
Masih `PHP`, namun versi yang Kami gunakan saat ini adalah `PHP versi 5.4.9`, dan library [`phpredis`](https://github.com/nicolasff/phpredis). Library ini `mengcompile` php library ke dalam modul `C`, dan ini lebih cepat dari library php redis yang lainnya.

###4. Sistem Operasi
`NoSQL` mayoritas hidup dalam lingkungan `*nix`, sangat jarang ditemukan `nosql` yang hidup di lingkungan `windows`. Server yang kami gunakan adalah `Ubuntu server 13`, dan ini hidup di dalam `virtualbox` di server `223.27.144.195`, menggunakan `32GB ram` dan `8 Core CPU`.

##Overview Aplikasi
###1. Tampilan Aplikasi
###1.1 Aplikasi Lama
> pake [ini](http://223.27.144.195:8081/info.php?admin=1&nuptk=9646764665200012&debug=1) untuk info guru yg lama, diambil pake resolusi standar leptop (1366x768) sama mobile (600x400)

###1.1.1 Tampilan Normal
###1.1.2 Tampilan Mobile
###1.2 Aplikasi baru
> pake [ini](http://223.27.144.195:8313/info.php?admin=1&nuptk=9646764665200012&debug=1) untuk info guru yg baru, tapi close dulu box yg warna kuning, diambil pake resolusi standar leptop (1366x768) sama mobile (600x400)
> Pake plugin chrome utk ngatur resolusinya "chrome responsive test plugin" googling itu

###1.2.1 Tampilan Normal
###1.2.2 Tampilan Mobile

###2. Benchmark
Tujuan maintenance ini adalah akses aplikasi semakin cepat dan aplikasi tidak pernah down. Untuk membuktikannya, Kami melakukan testing melalui `apache bench`. 

[`Apache bench`](http://httpd.apache.org/docs/2.2/programs/ab.html) adalah aplikasi untuk melakukan benchmark terhadap sebuah website. Dari `Apache Bench` dapat diketahui `Requests per Second` sebuah website. Sekuat apakah web server dapat diketahui dari hasil `Requests per Second` nya.

Berikut ini hasilnya :

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# ab -c 10 -n 10 "http://223.27.144.195:8081/info.php?admin=1&nuptk=9646764665200012&debug=1/"
    This is ApacheBench, Version 2.3 <$Revision: 655654 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/
    
    Benchmarking 223.27.144.195 (be patient).....done
    
    
    Server Software:        Apache/2.2.11
    Server Hostname:        223.27.144.195
    Server Port:            8081
    
    Document Path:          /info.php?admin=1&nuptk=9646764665200012&debug=1/
    Document Length:        20244 bytes
    
    Concurrency Level:      10
    Time taken for tests:   6.280 seconds
    Complete requests:      10
    Failed requests:        9
       (Connect: 0, Receive: 0, Length: 9, Exceptions: 0)
    Write errors:           0
    Total transferred:      206188 bytes
    HTML transferred:       202458 bytes
    Requests per second:    1.59 [#/sec] (mean)
    Time per request:       6279.519 [ms] (mean)
    Time per request:       627.952 [ms] (mean, across all concurrent requests)
    Transfer rate:          32.07 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        2    3   0.9      3       4
    Processing:  5808 6186 140.8   6234    6277
    Waiting:     3098 3574 178.4   3619    3716
    Total:       5811 6189 140.8   6236    6279
    
    Percentage of the requests served within a certain time (ms)
      50%   6236
      66%   6253
      75%   6275
      80%   6278
      90%   6279
      95%   6279
      98%   6279
      99%   6279
     100%   6279 (longest request)


Seperti yang terlihat diatas, aplikasi info guru yang lama hanya mampu menangani `1.59 RPS`.

Dan ini lah hasil benchmark aplikasi info guru yang baru (dengan data yang sama) :


    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# ab -c 10 -n 10 "http://223.27.144.201:8888/infoguru.php?nuptk=9646764665200012&tgl_lahir=19860314&debug=1/"
    This is ApacheBench, Version 2.3 <$Revision: 655654 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/
    
    Benchmarking 223.27.144.201 (be patient).....done
    
    
    Server Software:        Apache/2.2.22
    Server Hostname:        223.27.144.201
    Server Port:            8888
    
    Document Path:          /infoguru.php?nuptk=9646764665200012&tgl_lahir=19860314&debug=1/
    Document Length:        13966 bytes
    
    Concurrency Level:      10
    Time taken for tests:   0.080 seconds
    Complete requests:      10
    Failed requests:        8
       (Connect: 0, Receive: 0, Length: 8, Exceptions: 0)
    Write errors:           0
    Total transferred:      141562 bytes
    HTML transferred:       139652 bytes
    Requests per second:    125.46 [#/sec] (mean)
    Time per request:       79.704 [ms] (mean)
    Time per request:       7.970 [ms] (mean, across all concurrent requests)
    Transfer rate:          1734.47 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        4    7   1.5      7       9
    Processing:    51   56   5.5     56      70
    Waiting:       34   47  10.5     44      67
    Total:         58   63   6.2     62      79
    
    Percentage of the requests served within a certain time (ms)
      50%     62
      66%     63
      75%     64
      80%     66
      90%     79
      95%     79
      98%     79
      99%     79
     100%     79 (longest request)


Dapat dilihat, peningkatan yang sangat signifikan. Didapat `125 RPS`.

###3. Struktur Data
Struktur data `redis` dan `sql server` sangat jauh berbeda, antara rdbms yang berbasis sql dan nosql yang berbasis key value store. Maka metode penyimpanannya data nya pun sangat jauh berbeda.

Dalam Aplikasi ini, Kami menggunakan `5 redis instances` yang terdiri dari `5 daemon redis` dengan port : `6379`, `6380`, `6381`, `6382`, `6383` dan `6384`.

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# ps -ef | grep redis
    root       816     1  0 Sep12 ?        06:08:01 /usr/bin/redis-server /data/r_ptk/redis.conf
    root       820     1  0 Sep12 ?        05:55:48 /usr/bin/redis-server /data/r_mengajar/redis.conf
    root       826     1  1 Sep12 ?        06:21:10 /usr/bin/redis-server /data/r_index/redis.conf
    root       830     1  0 Sep12 ?        05:56:40 /usr/bin/redis-server /data/r_other/redis.conf
    root       835     1  0 Sep12 ?        05:49:09 /usr/bin/redis-server /data/r_report/redis.conf
    root       839     1  0 Sep12 ?        05:47:57 /usr/bin/redis-server /data/r_tunjangan/redis.conf
    root     15885 14667  0 15:36 pts/1    00:00:00 grep redis
    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# netstat -ntpl | grep redis
    tcp        0      0 0.0.0.0:6379            0.0.0.0:*               LISTEN      816/redis-server
    tcp        0      0 0.0.0.0:6380            0.0.0.0:*               LISTEN      820/redis-server
    tcp        0      0 0.0.0.0:6381            0.0.0.0:*               LISTEN      826/redis-server
    tcp        0      0 0.0.0.0:6382            0.0.0.0:*               LISTEN      830/redis-server
    tcp        0      0 0.0.0.0:6383            0.0.0.0:*               LISTEN      835/redis-server
    tcp        0      0 0.0.0.0:6384            0.0.0.0:*               LISTEN      839/redis-server


###3.1 Instance Redis PTK

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6379 randomkey
    "t_ptk:1560750652210092:lYrOnCQoEeK1nYl5E6RArQ"

      t_ptk    1560750652210092  lYrOnCQoEeK1nYl5E6RArQ
        |             |                   |
        V             V                   V
      table         NUPTK               PTK ID

###3.2 Instance Redis Mengajar

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6380 randomkey       
    "t_mengajar:7454760661300033:XVdAgPUuEeCnmfnivLgxCw:2"


      t_mengajar 7454760661300033  XVdAgPUuEeCnmfnivLgxCw 2
        |             |                   |               |
        V             V                   V               V
      table         NUPTK               PTK ID     Jumlah mengajar

###3.3 Instance Redis Index

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6381 randomkey
    "login:0538742644200063:19640612"

      login    0538742644200063     19640612
        |             |                |               
        V             V                V               
      table         NUPTK           Password     

###3.4 Instance Redis Rombel & Tunjangan Profesi

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6382 randomkey
    "t_rombel:3a7bMKYeEeK-j6G8W8iFbQ:11"

     t_rombel    3a7bMKYeEeK-j6G8W8iFbQ      11
        |                   |                |               
        V                   V                V               
      table            Id sekolah         Jumlah Rombel

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6382 randomkey
    "t_tunjangan_profesi_new:4041757661300003:15077532"

###3.5 Instance Redis Report

    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6383 randomkey
    "t_report:super_duplicate"
    root@ubuntu13:/var/www/campur/dokumentasi_infoguru# redis-cli -p 6383 randomkey
    "t_report_done:hash"
    
Berisi tool manajemen redis. Aplikasi dapat dilihat di link [`ini`](http://223.27.144.201:8888/php_redis/phpRedisAdmin/)

###4. Code Snippet
###4.1 Connection

    $redisPTK = new Redis() or die("Gagal memuat modul redis.");
    $redisPTK->connect('127.0.0.1', 6379);
    
    $redisMengajar = new Redis() or die("Gagal memuat modul redis.");
    $redisMengajar->connect('127.0.0.1', 6380);
    
    $redisIndex = new Redis() or die("Gagal memuat modul redis.");
    $redisIndex->connect('127.0.0.1', 6381);
    
    $redisOther = new Redis() or die("Gagal memuat modul redis.");
    $redisOther->connect('127.0.0.1', 6382);
    
    $redisReport = new Redis() or die("Gagal memuat modul redis.");
    $redisReport->connect('127.0.0.1', 6383);
    
    $redisTunjangan = new Redis() or die("Gagal memuat modul redis.");
    $redisTunjangan->connect('127.0.0.1', 6384);

###4.2 Login Process

    /**
     * login process, NORMAL USER
     */
    if (isset($_REQUEST['nuptk']) && isset($_REQUEST['tgl_lahir']) ){
    
        $ArrKeys['login'] = $redisIndex->hgetall('login:'.$_REQUEST['nuptk'].':'.$_REQUEST['tgl_lahir']);
        if (count($ArrKeys['login']) == 0){
            die('<center>NUPTK atau Tanggal Lahir Salah<br><a href="index.html">Halaman Login</a></center>');
        }
        // else {
        // }
    }
    /**
     * login process, ADMIN
     */
    elseif ( (isset($_REQUEST['nuptk']) && isset($_REQUEST['admin']) && ($_REQUEST['admin'] == 1) ) ){
        $ArrKeys['admin'] = $redisIndex->keys('login:'.$_REQUEST['nuptk'].'*');
        $ArrKeys['login'] = $redisIndex->hgetall($ArrKeys['admin'][0]);
    }
    
###4.3 Migrate SQL SERVER -> REDIS 6379 & 6380 & 6381

    /**
     * UPDATE DATA FROM SQL SERVER TO REDIS
     * @param  [type] $redisPTK
     * @param  [type] $redisMengajar
     * @param  [type] $redisOther
     * @param  [type] $nuptk
     * @return [type]
     */
    function updateRedisOneNUPTK($redisPTK, $redisMengajar, $redisIndex, $nuptk){
        // some variable
        $lastId = '';
        $lastCounter = 1;
        $startRedis = microtime(true);
        $last_update_id = 0;
        $redis_arr = array();
    
        // INSERT REDIS PTK
        $query_ptk = 'select * from t_ptk where nuptk_link = \''.$nuptk.'\'';
        // echo $query_ptk."\n";
        $result_ptk = mssql_query($query_ptk);
        // loop sql result PTK
        while ($row = mssql_fetch_assoc($result_ptk)) {
            // assign key Redis PTK
            $nuptk = $row['nuptk_link'] ? $row['nuptk_link'] : "no_nuptk";
    
            // set redis t_ptk key
            $redis_arr['t_ptk'] = array(
                "t_ptk_id" => $row['t_ptk_id'],
                "kode" => $row['kode'],
                "nama" => $row['nama'],
                "jenis_kelamin" => $row['jenis_kelamin'],
                "ijazah_terakhir_id" => $row['ijazah_terakhir_id'],
                "tahun_ijazah_terakhir" => $row['tahun_ijazah_terakhir'],
                "gelar_akademik_depan_id" => $row['gelar_akademik_depan_id'],
                "gelar_akademik_belakang_id" => $row['gelar_akademik_belakang_id'],
                "niy_nigk" => $row['niy_nigk'],
                "nuptk" => $row['nuptk'],
                "tempat_lahir" => $row['tempat_lahir'],
                "tgl_lahir" => $row['tgl_lahir'],
                "nik" => $row['nik'],
                "agama_id" => $row['agama_id'],
                "status_perkawinan" => $row['status_perkawinan'],
                "jumlah_anak" => $row['jumlah_anak'],
                "nama_ibu_kandung" => $row['nama_ibu_kandung'],
                "alamat_jalan" => $row['alamat_jalan'],
                "rt" => $row['rt'],
                "rw" => $row['rw'],
                "nama_desa_kelurahan" => $row['nama_desa_kelurahan'],
                "kode_pos" => $row['kode_pos'],
                "kecamatan_id" => $row['kecamatan_id'],
                "kabupaten_kota_id" => $row['kabupaten_kota_id'],
                "no_telepon_rumah" => $row['no_telepon_rumah'],
                "no_hp" => $row['no_hp'],
                "email" => $row['email'],
                "status_kepegawaian_id" => $row['status_kepegawaian_id'],
                "tmt_sekolah" => $row['tmt_sekolah'],
                "jabatan_ptk_id" => $row['jabatan_ptk_id'],
                "tmt_jabatan" => $row['tmt_jabatan'],
                "jabatan_ptk_sebelumnya_id" => $row['jabatan_ptk_sebelumnya_id'],
                "sertifikasi_jabatan" => $row['sertifikasi_jabatan'],
                "tahun_sertifikasi_jabatan" => $row['tahun_sertifikasi_jabatan'],
                "nomor_sertifikat" => $row['nomor_sertifikat'],
                "nip" => $row['nip'],
                "tmt_pns" => $row['tmt_pns'],
                "pangkat_golongan_id" => $row['pangkat_golongan_id'],
                "tmt_pangkat_gol" => $row['tmt_pangkat_gol'],
                "kode_sertifikasi_bidang_studi_guru_id" => $row['kode_sertifikasi_bidang_studi_guru_id'],
                "kode_program_keahlian_laboran_id" => $row['kode_program_keahlian_laboran_id'],
                "sudah_lisensi_kepala_sekolah" => $row['sudah_lisensi_kepala_sekolah'],
                "jenjang_kepengawasan_id" => $row['jenjang_kepengawasan_id'],
                "pengawas_bidang_rumpun_id" => $row['pengawas_bidang_rumpun_id'],
                "pengawas_mapel_id" => $row['pengawas_mapel_id'],
                "jumlah_sekolah_binaan" => $row['jumlah_sekolah_binaan'],
                "pernah_mengikuti_diklat_kepengawasan" => $row['pernah_mengikuti_diklat_kepengawasan'],
                "nama_suami_istri" => $row['nama_suami_istri'],
                "pekerjaan_id" => $row['pekerjaan_id'],
                "nip_suami_istri" => $row['nip_suami_istri'],
                "sekolah_id" => $row['sekolah_id'],
                "sim_nuptk" => $row['sim_nuptk'],
                "status" => $row['status'],
                "status_data" => $row['status_data'],
                "jabatan_fungsional" => $row['jabatan_fungsional'],
                "lembaga_pengangkat" => $row['lembaga_pengangkat'],
                "nomor_sk_pengangkatan" => $row['nomor_sk_pengangkatan'],
                "tmt_pengangkatan" => $row['tmt_pengangkatan'],
                "sumber_gaji" => $row['sumber_gaji'],
                "nomor_sk_kgb" => $row['nomor_sk_kgb'],
                "tmt_kgb" => $row['tmt_kgb'],
                "nama_nuptk" => $row['nama_nuptk'],
                "tgl_lahir_nuptk" => $row['tgl_lahir_nuptk'],
                "nama_ijazah_terakhir" => $row['nama_ijazah_terakhir'],
                "nrg" => $row['nrg'],
                "nama_nrg" => $row['nama_nrg'],
                "tgl_lahir_nrg" => $row['tgl_lahir_nrg'],
                "nama_bidang_studi_sertifikasi" => $row['nama_bidang_studi_sertifikasi'],
                "kode_bidang_studi_sertifikasi" => $row['kode_bidang_studi_sertifikasi'],
                "bidang_studi_sertifikasi_id" => $row['bidang_studi_sertifikasi_id'],
                "tgl_lulus_sertifikasi" => $row['tgl_lulus_sertifikasi'],
                "nama_sekolah" => $row['nama_sekolah'],
                "status_sekolah" => $row['status_sekolah'],
                "nama_jenis_sekolah" => $row['nama_jenis_sekolah'],
                "jenis_sekolah_id" => $row['jenis_sekolah_id'],
                "nama_kabupaten_kota" => $row['nama_kabupaten_kota'],
                "kabupaten_kota_sekolah_id" => $row['kabupaten_kota_sekolah_id'],
                "nama_propinsi" => $row['nama_propinsi'],
                "propinsi_sekolah_id" => $row['propinsi_sekolah_id'],
                "nama_pangkat_golongan" => $row['nama_pangkat_golongan'],
                "nama_status_kepegawaian" => $row['nama_status_kepegawaian'],
                "is_pns" => $row['is_pns'],
                "nama_tugas_tambahan" => $row['nama_tugas_tambahan'],
                "tugas_tambahan_id" => $row['tugas_tambahan_id'],
                "jam_tugas_tambahan" => $row['jam_tugas_tambahan'],
                "jumlah_jam_mengajar" => $row['jumlah_jam_mengajar'],
                "jumlah_jam_mengajar_sesuai" => $row['jumlah_jam_mengajar_sesuai'],
                "jumlah_jam_mengajar_sesuai_ktsp" => $row['jumlah_jam_mengajar_sesuai_ktsp'],
                "total_jam_mengajar_sesuai" => $row['total_jam_mengajar_sesuai'],
                "masa_kerja_tahun" => $row['masa_kerja_tahun'],
                "masa_kerja_bulan" => $row['masa_kerja_bulan'],
                "tgl_pensiun" => $row['tgl_pensiun'],
                "sedang_kuliah" => $row['sedang_kuliah'],
                "bertugas_di_wilayah_terpencil" => $row['bertugas_di_wilayah_terpencil'],
                "last_update" => $row['last_update'],
                "nama_rekening" => $row['nama_rekening'],
                "no_rekening" => $row['no_rekening'],
                "nama_bank" => $row['nama_bank'],
                "cabang_bank" => $row['cabang_bank'],
                "satuan_pendidikan_formal" => $row['satuan_pendidikan_formal'],
                "fakultas" => $row['fakultas'],
                "bidang_studi_id" => $row['bidang_studi_id'],
                "jenjang_pendidikan_id" => $row['jenjang_pendidikan_id'],
                "tahun_masuk" => $row['tahun_masuk'],
                "semester" => $row['semester'],
                "status_nuptk" => $row['status_nuptk'],
                "status_syarat_t_profesi" => $row['status_syarat_t_profesi'],
                "status_syarat_t_fungsional" => $row['status_syarat_t_fungsional'],
                "status_syarat_t_kualifikasi" => $row['status_syarat_t_kualifikasi'],
                "status_syarat_t_khusus" => $row['status_syarat_t_khusus'],
                "nuptk_link" => $row['nuptk_link'],
                "nuptk_link_level" => $row['nuptk_link_level'],
                "nama_instansi_nuptk" => $row['nama_instansi_nuptk'],
                "memenuhi_syarat_tunjangan_profesi" => $row['memenuhi_syarat_tunjangan_profesi'],
                "jumlah_jam_mengajar_diluar_dikdas" => $row['jumlah_jam_mengajar_diluar_dikdas'],
                "nuptk_link2" => $row['nuptk_link2']
            );
            
            // set key redis key ptk
            $redis_arr['key_t_ptk'] = 't_ptk:'.$nuptk.':'.$row['t_ptk_id'];
    
            // assign key Redis Index & Mapper (login)
            // cek, kalo nuptk ga ada, pake ptk id aja
            if ($nuptk == 'no_nuptk'){
                $redis_arr['key_t_login'] = 'login:'.$row['t_ptk_id'].':'.format_date($row['tgl_lahir']);
            }
            else {
                $redis_arr['key_t_login'] = 'login:'.$nuptk.':'.format_date($row['tgl_lahir']);            
            }
    
            // assign array redis login
            $redis_arr['t_login'] = array(
                $row['t_ptk_id'] => getSize($redis_arr['t_ptk']) 
            );
    
            // SET TO REDIS
            // cek kalo last update lebih baru, ga perlu ditambahin
            // if (strtotime($row['last_update']) > strtotime($last_update_id)){            
            //     $redisPTK->hmSet($redis_arr['key_t_ptk'], $redis_arr['t_ptk']);
            //     // echo "Nambahin ptk baru\n";
            //     $last_update_id = $row['last_update'];          
            //     // echo "Last Update = " . strtotime($last_update_id) . "\n";
            // }
            
            // set redis login
            $redisIndex->hmSet($redis_arr['key_t_login'], $redis_arr['t_login']);
            // set redis PTK
            $redisPTK->hmSet($redis_arr['key_t_ptk'], $redis_arr['t_ptk']);
        } // end while PTK
    
    
        // INSERT REDIS MENGAJAR
        $query_mengajar = 'select * from t_mengajar where nuptk = \''.$nuptk.'\' order by t_ptk_id';
        // echo $query_mengajar."\n";
        $result_mengajar = mssql_query($query_mengajar);
        // loop sql result
        // set some var
        $c = 1;
        while ($row = mssql_fetch_array($result_mengajar)) {
            // assign key redis Mengajar
            $redis_arr['t_mengajar'] = array(
                "t_ptk_id" =>$row['t_ptk_id'], 
                "nuptk" =>$row['nuptk'], 
                "nama" =>$row['nama'], 
                "kode_bidang_studi_sertifikasi" =>$row['kode_bidang_studi_sertifikasi'], 
                "tahun_sertifikasi" =>$row['tahun_sertifikasi'], 
                "sekolah_id" =>$row['sekolah_id'], 
                "nama_sekolah" =>$row[6], 
                "rombongan_belajar_id" =>$row['rombongan_belajar_id'], 
                "tingkat" =>$row['tingkat'], 
                "bidang_studi_id" =>$row['bidang_studi_id'], 
                "nama_matpel_diajarkan" =>$row['nama_matpel_diajarkan'], 
                "kode_matpel_diajarkan" =>$row['kode_matpel_diajarkan'], 
                "jjm" =>$row['jjm'], 
                "jjm_ktsp" =>$row['jjm_ktsp'], 
                "is_linier" =>$row['is_linier'], 
                "jumlah_siswa" =>$row['jumlah_siswa'], 
                "tahun_ajaran_id" =>$row['tahun_ajaran_id'], 
                "semester" =>$row['semester'], 
                "jjm_linier" =>$row['jjm_linier'], 
                "jjm_shared" =>$row['jjm_shared'], 
                "jumlah_guru_paralel" =>$row['jumlah_guru_paralel'], 
                "nama_rombel" =>$row['nama_rombel'], 
                "jumlah_ganda" =>$row['jumlah_ganda'], 
                "jjm_dibatas_ktsp" =>$row['jjm_dibatas_ktsp'], 
                "jjm_rombel" =>$row['jjm_rombel'], 
                "is_jjm_rombel_normal" =>$row['is_jjm_rombel_normal'], 
                "is_rombel_rsbi" =>$row['is_rombel_rsbi']
            );
    
            // MENGAJAR
            if ($lastId == $row['t_ptk_id']){
                // echo "ptk id sama \n";
                $lastCounter++;
                // format key redis
                $redis_arr['key_t_mengajar'] = 't_mengajar:'.$nuptk.':'.$row['t_ptk_id'].':'.$lastCounter;
                // echo $redis_arr['key_t_mengajar']. "\n";
                //set master t_mengajar
                $redisMengajar->hmSet('t_mengajar:'.$nuptk.':'.$row['t_ptk_id'], array("jumlah_key" => $lastCounter));
                // set last id
                $lastId = $row['t_ptk_id'];
                // set nuptk ini mengajar pake ptk_id ini
                $redisMengajar->sadd('t_mengajar:'.$nuptk, $row['t_ptk_id']);
                #          echo $row['t_ptk_id']."\n";
            }
            else {
                // format key redis
                $redis_arr['key_t_mengajar'] = 't_mengajar:'.$nuptk.':'.$row['t_ptk_id'].':'.$c;
                #          echo $redis_arr['key_t_mengajar']. "\n";          
                // set last id
                $lastId = $row['t_ptk_id'];
                // set informasi jumlah key       
                $lastCounter = 1;
                // set master t_mengajar
                $redisMengajar->hmSet('t_mengajar:'.$nuptk.':'.$row['t_ptk_id'], array("jumlah_key" => $lastCounter));
                // set nuptk ini mengajar pake ptk_id ini
                $redisMengajar->sadd('t_mengajar:'.$nuptk, $row['t_ptk_id']);
    
                // unset($redis_arr);
                // mssql_free_result($result_mengajar);
                // unset($result_mengajar);
            }
            // set redis mengajar
            $redisMengajar->hmSet($redis_arr['key_t_mengajar'], $redis_arr['t_mengajar']);
        } //end while loop mengajar
        return $redis_arr['t_login'];
    } // end function

###4.4 Report

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="assets/bootstrap-3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="assets/bootstrap-3.0.0/js/jquery-1.10.1.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-3.0.0/js/bootstrap.min.js"></script>
        <title>Reporting NUPTK</title>
    </head>
    <body>
    <?php
    
    // cek kalo ada request NUPTK
    if (isset($_REQUEST['nuptk']) && !empty($_REQUEST['nuptk']) && is_numeric($_REQUEST['nuptk']) ){
    
        //includes
        include 'redis-lib.php';
        include 'validasi-lib.php';
    
        // redis connect
        $redis = getRedis(6383);
    
    #    echo $_REQUEST['nuptk']; 
        if (isset($_REQUEST['laporan']) && !empty($_REQUEST['laporan'])){
            // redis key
            $key_hash = 't_report_after_fix:hash:';
            $key_set = 't_report_after_fix:set';
            $key = 't_report_after_fix:hash';
    
            // clean input
            $nuptk = checkLength(isNumber($_REQUEST['nuptk']), 25, $minLength = 0);
    #        printr($nuptk);
    
            // redis process
            $redis->sadd($key_set, $nuptk);
            // $redis->set($key_hash.$nuptk, $_REQUEST['laporan']);
            $redis->hmSet($key, array ($nuptk => $_REQUEST['laporan']));
            
            die('<center>We\'re working on it.<br><a href="index.html">Halaman Login</a></center>');
        }
    }
    ?>
    <div class="container">
        <center><legend>Report NUPTK</legend></center>
        <form method="POST" class="nuptk" action="report.php" accept-charset="UTF-8">
            <p class="help-block">NUPTK</p>
            <input type="text" id="nuptk" class="form-control" name="nuptk" placeholder="NUPTK" value="<?=$_REQUEST['nuptk']?>">
            <p class="help-block">Pesan tambahan</p>
            <textarea rows="3" id="pesan" class="form-control" name="laporan" placeholder="Pesan"></textarea>
            <p class="help-block"></p>
            <blockquote>
                <p class="text-warning">* Perbaiki data guru jika diperlukan melalui Dapodik Dikdas. Data akan terupdate dalam beberapa hari.</p>
                <p class="text-danger">* HARAP DIPAHAMI BAHWA MEKANISME UPDATE DATA HANYA BISA DILAKUKAN MELALUI APLIKASI DAPODIK.</p>
                <p class="text-danger">* KAMI <strong><u>TIDAK AKAN MENJAWAB</u></strong> REQUEST UPDATE DATA.</p>
            </blockquote>
            <p class="help-block"></p>        
            <button type="submit" name="submit" class="btn btn-warning btn-block" id="submit">Report</button>
        </form>
    </div>
    </div>
    </body>
    </html>

###4.5 Info Rombel

    <?php
    include 'redis-lib.php';
    
    if (isset($_REQUEST['rombel_id']) && !empty($_REQUEST['rombel_id'])){
        // Start stopwatch
        startStopwatch();
    
        // ambil data master jumlah key rombel
        $rombelKey = 't_rombel:'.$_REQUEST['rombel_id'];
      $redisRombel = getRedis(6382);
      $cntRombel = $redisRombel->hgetall($rombelKey);
    
        // looping sebanyak jumlah rombel dari redis
        $arrRombel = array();
      for ($i = 1; $i <= $cntRombel['jumlah_key']; $i++){
          $rombels = 't_rombel:'.$_REQUEST['rombel_id'].':'.$i;
          $arrRombel[] = $redisRombel->hgetall($rombels);
      }
      
        // kalo minta data
      if (isset($_REQUEST['data']) && ($_REQUEST['data'] == 1)){
          printr($arrRombel);
      }
    
        // kalo debug
        if ($_REQUEST['debug'] == 1){
            endStopwatch("Waktu load: ");
            echo "<hr>\n".$swTime." detik<br>\n";
        }
      
    }
    else {
      die ('whoops');
    }
    
    ?>
    <!DOCTYPE html>
    <html>
    <head>
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href='bootstrap/img/favicon.png' rel='shortcut icon' />
      <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>  
      <title>Info Rombel</title>
    </head>
    <body>
      <div class="span8">
        <div class="span8">
            <h2>Daftar Guru Mengajar Rombel</h2>
            <h5>Nama Rombel: <?=$cntRombel['nama']?></h5>
            <h5>Sekolah: <?=$cntRombel['nama_sekolah']?></h5>
            <h5>Kelebihan Jam Rombel: <?=$_REQUEST['jjm_rombel']?></h5>
            <h5>Rombel Normal: <?=$_REQUEST['is_jjm_rombel_normal'] ? "Ya" : "Tidak"?></h5>
          <table class="table table-striped">
              <thead>
                <tr class="text-center">
                    <th class="text-center">Mata Pelajaran</th>
                    <th>Nama Guru</th>
                    <th>JJM</th>
                    <th>JJM<br>KTSP</th>
                </tr>
            </thead>   
            <tbody>
            <?php
                  $totJJM = 0;
                  $totJJMKTSP = 0;        
                    foreach ($arrRombel as $rombels){
            ?>
              <tr>
                  <td><?php echo $rombels['kode_matpel_diajarkan']."-".$rombels['nama_matpel_diajarkan']; ?></td>
                  <td><?php echo $rombels['nama']; ?></td>
                  <td><?php echo $rombels['jjm']; $totJJM += $rombels['jjm']; ?></td>
                  <td><?php echo $rombels['jjm_ktsp']; $totJJMKTSP += $rombels['jjm_ktsp']; ?></td>
              </tr>
            <?php
              }
            ?>
                    <tr>
                    <td colspan="2"><strong>Jumlah Jam Mengajar Rombel</strong></td>
                      <td><strong><?php echo $totJJM; ?></strong></td>
                      <td><strong><?php echo $totJJMKTSP; ?></strong></td>
                  </tr>
            </tbody>        
          </table>
                <blockquote>
                  <p class="text-error">* Rombel dinyatakan tidak normal jika jumlah total jam mengajar di rombel tersebut tidak wajar.</p>
                  <p class="text-error">* Ketidakwajaran jumlah dapat diakibatkan mata pelajaran yang sama diajarkan oleh lebih dari satu guru.</p>
                </blockquote>     
        </div>
      </div>  
    </body>
    </html>


###4.6 Info Guru 

    <?php
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(-1);
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="assets/bootstrap-3.0.0/css/bootstrap.min.css" rel="stylesheet">
      <link href='assets/bootstrap-3.0.0/img/favicon.png' rel='shortcut icon' />
      <title>Verifikasi Data Guru</title>
    </head>
    <body>
    <?php
    if (isset($_REQUEST['logout']) ){
    #    die;
        $loc = "http://".$_SERVER['HTTP_HOST']."/info/";
    #    echo $loc;
        header("Location: $loc");
    }
    else{
        require 'get_redis.php';
        require 'validasi-lib.php';
    
        if (isset($_REQUEST['data']) && ($_REQUEST['data'] == 1)){
            printr($ArrKeys);
    #        printr($_SERVER);
        }
    }
    
    ?>
    
    <div class="container">
      <br>
        <div class="bs-docs-example">
          <div class="alert alert-block alert-warning fade in">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4 class="alert-heading">Perhatian!!</h4>
            <p>Kami sedang dalam proses peningkatan kinerja sistem & updating data, jika data belum terupdate silahkan klik tombol dibawah ini.</p>
            <p>
              <a class="btn btn-warning" id="alert-danger" href="#">Update data</a>
            </p>
          </div>
        </div>
        <marquee><h4 class="text-warning">* <?php echo $row_pengumuman['set_value'];?></h4></marquee>
      <div id="jsonresult"></div>
      <h3>Info Data PTK</h3>
      <?php
      if (isset($_REQUEST['debug']) &&($_REQUEST['debug'] == 1))
      {
    
      ?>
      <h4><a href="http://223.27.144.195:8313/info.php?admin=1&nuptk=<?php echo $ArrKeys['result_ptk']['nuptk_link']; ?>&debug=1" target="_blank"> Link Info Guru</a> </h4>
      <h4><a href="http://223.27.144.201:8888/info_v2/infoguru.php?admin=1&nuptk=<?php echo $ArrKeys['result_ptk']['nuptk_link']; ?>&debug=1" target="_blank"> Link Info Guru V2</a> </h4>
      <?php
      }
      ?>
      <h5>Menurut data yang diupload ke Pendataan Pendidikan Dasar
    Tanggal pengiriman terakhir: <span id="last_update"><?=format_date2($ArrKeys['result_ptk']['last_update'])?></span> pukul <?=format_date3($ArrKeys['result_ptk']['last_update'])?></h5>
      <div>
      <div class="table-responsive">
      <table class="table table-striped table-condensed table-bordered">
          <thead>
            <tr>
                <th>No</th>
                <th>Field</th>
                <th>Data</th>
                <th>Status</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
              <td>Nama</td>
              <td><?php echo $ArrKeys['result_ptk']['nama']; ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>2</td>
              <td>NIP</td>
              <td><?php echo $ArrKeys['result_ptk']['nip']; ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>3</td>
              <td>Nama Kabupaten Kota</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_kabupaten_kota']; ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>4</td>
              <td>Nama Propinsi</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_propinsi']; ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>5</td>
              <td>Nama Sekolah Induk</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_sekolah']; ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>6</td>
              <td>Ijazah Terakhir</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_ijazah_terakhir']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['nama_ijazah_terakhir'], "Ijazah"); ?>
          </tr>
          <tr>
            <td>7</td>
              <td>NUPTK</td>
              <td><?php echo $ArrKeys['result_ptk']['nuptk_link']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['nuptk_link'], "NUPTK"); ?>
          </tr>
          <?php
        $tgl = date_parse($ArrKeys['result_ptk']['tgl_lahir']);
        $ArrKeys['result_ptk']['tgl_lahir'] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];
          ?>
          <tr>
            <td>8</td>
              <td>Tanggal Lahir</td>
              <td><?php echo $ArrKeys['result_ptk']['tgl_lahir']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['tgl_lahir'], "Tanggal Lahir"); ?>
          </tr>
          <tr>
            <td>9</td>
              <td>Nama Status Kepegawaian</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_status_kepegawaian']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['nama_status_kepegawaian'], "Status kepegawaian"); ?>
          </tr>
          <tr>
            <td>10</td>
              <td>Nama Pangkat Golongan</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_pangkat_golongan']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['nama_pangkat_golongan'], "Pangkat golongan"); ?>
          </tr>
          <?php
        $tgl = date_parse($ArrKeys['result_ptk']['tmt_pangkat_gol']);
        $ArrKeys['result_ptk']['tmt_pangkat_gol'] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];
          ?>
          <tr>
            <td>11</td>
              <td>Tmt Pangkat Golongan</td>
              <td><?php echo $ArrKeys['result_ptk']['tmt_pangkat_gol']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['tmt_pangkat_gol'], "Tgl TMT"); ?>
          </tr>
          <tr>
            <td>12</td>
              <td>Nama Tugas Tambahan</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_tugas_tambahan']; ?></td>
              <!-- <?php echo validate($ArrKeys['result_ptk']['nama_tugas_tambahan'], "Tugas tambahan"); ?> -->
              <td></td>
              <td></td>
          </tr>
          <tr>
            <td>13</td>
              <td>Masa Kerja (Tahun)</td>
              <td><?php echo $ArrKeys['result_ptk']['masa_kerja_tahun']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['masa_kerja_tahun'], "Masa kerja", "ada", "kosong"); ?>
          </tr>
          <tr>
            <td>14</td>
              <td>Sedang Kuliah</td>
              <td><?php echo $ArrKeys['result_ptk']['sedang_kuliah']; ?></td>
              <!-- <?php echo validate($ArrKeys['result_ptk']['sedang_kuliah'], "Data sedang kuliah", "ada", "kosong"); ?> -->
              <td></td>
              <td></td>
          </tr>
          <tr>
            <td>15</td>
              <td>Bertugas di Wilayah Khusus</td>
              <td><?php echo $ArrKeys['result_ptk']['bertugas_di_wilayah_terpencil']; ?></td>
              <!-- <?php echo validate($ArrKeys['result_ptk']['bertugas_di_wilayah_terpencil'], "Data bertugas di wilayah khusus", "ada", "kosong"); ?> -->
              <td></td>
              <td></td>
          </tr>
          <tr>
            <td>16</td>
              <td>Email</td>
              <td><?php echo $ArrKeys['result_ptk']['email']; ?></td>
              <!-- <?php echo validate($ArrKeys['result_ptk']['email'], "Email", "valid", "tidak valid"); ?> -->
              <td></td>
              <td></td>
          </tr>
          <?php
            $redisGajiPokok = getRedis(6382);
            $ArrKeys['result_ptk']['gaji_pokok'] = $redisGajiPokok->hgetall('t_gaji_pokok:'.$ArrKeys['result_ptk']['pangkat_golongan_id'].':'.$ArrKeys['result_ptk']['masa_kerja_tahun']);
            $gaji_pokok = $ArrKeys['result_ptk']['gaji_pokok']['gaji_pokok'];
            if ($gaji_pokok == 0){
                // kalo pangkat_golongan_id diantara 5 dan 8 berarti ganjil
                if (($ArrKeys['result_ptk']['pangkat_golongan_id'] >= 5) && ($ArrKeys['result_ptk']['pangkat_golongan_id'] <= 8)){
                    $masa_kerja = $ArrKeys['result_ptk']['masa_kerja_tahun'] - 1;
                    $gaji_pokok = $redisGajiPokok->hgetall('t_gaji_pokok:'.$ArrKeys['result_ptk']['pangkat_golongan_id'].':'.$masa_kerja)['gaji_pokok'];
                }
                else {
                    $masa_kerja = $ArrKeys['result_ptk']['masa_kerja_tahun'] - 1;
                    $gaji_pokok = $redisGajiPokok->hgetall('t_gaji_pokok:'.$ArrKeys['result_ptk']['pangkat_golongan_id'].':'.$masa_kerja)['gaji_pokok'];
                }
            }
          ?>
          <tr>
            <td>17</td>
              <td>Gaji Pokok</td>
              <td>Rp. <?php echo nf($gaji_pokok); ?></td>
              <td><span class="label btn-info">Info</span></td>
              <td></td>
          </tr>
          <tr>
            <td>18</td>
              <td>Status</td>
              <td><?php echo $ArrKeys['result_ptk']['status']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['status'], "Status PTK di sekolah tsb", "aktif", "tidak aktif"); ?>
          </tr>
          <tr>
            <td>19</td>
              <td>Tahun Pensiun</td>
              <td><?php echo retTahun($ArrKeys['result_ptk']['tgl_pensiun']); ?></td>
              <?php echo validate(retTahun($ArrKeys['result_ptk']['tgl_pensiun']), "pensiun", "belum terlewati", "sudah terlewati"); ?>
          </tr>
          <tr>
            <td>20</td>
              <td>Status Kepegawaian ID</td>
              <td><?php echo $ArrKeys['result_ptk']['nama_status_kepegawaian']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['nama_status_kepegawaian'], "Status Kepegawaian", "memenuhi syarat", "belum memenuhi syarat"); ?>
          </tr>
          <tr>
            <td>21</td>
              <td>Total Jam Mengajar Sesuai</td>
              <td><?php echo $ArrKeys['result_ptk']['total_jam_mengajar_sesuai']; ?></td>
              <?php echo validate($ArrKeys['result_ptk']['total_jam_mengajar_sesuai'], "JJM", "cukup (>=24)", "belum cukup (<=24)"); ?>
          </tr>
    
        </tbody>
      </table>
      </div>
      </div>
      <div class="clearfix"></div>
    
      <!-- RINCIAN JJM -->
      <h3>Rincian Jam Mengajar</h3>
      <div class="table-responsive">
      <table class="table table-striped table-condensed table-bordered">
          <thead>
            <tr>
                <th>Mata Pelajaran</th>
                <th>Tahun/Smt</th>
                <th>JJM</th>
                <th>JJM <br> KTSP</th>
                <th>JJM <br> Linier</th>
                <th>Sekolah</th>
                <th>Normal</th>
                <th>Info</th>
            </tr>
        </thead>
        <tbody>
          <?php
          $totJJM = 0;
          $totJJMKTSP = 0;
          $totJJMLinier = 0;
          $namaSekolah = '';
    
          // cek kalo nuptk mengajar hasilnya lebih dari 0
          if (count($ArrKeys['result_mengajar']) > 0 ){
          // looping foreach mengajar
            foreach ($ArrKeys['result_mengajar'] as $dataMengajar){
                $namaSekolah = $dataMengajar['nama_sekolah'];
            ?>
            <tr>
              <td><?php echo $dataMengajar['kode_matpel_diajarkan']."-". $dataMengajar['nama_matpel_diajarkan'];?></td>
                <td><?php echo "Smt ".$dataMengajar['semester']." Thn ".$dataMengajar['tahun_ajaran_id']." <br>(Rombel: ".$dataMengajar['nama_rombel'].")";?></td>
                <td><?php echo $dataMengajar['jjm']; $totJJM += $dataMengajar['jjm']; ?></td>
                <td><?php echo $dataMengajar['jjm_ktsp']; $totJJMKTSP += $dataMengajar['jjm_ktsp'];?></td>
                <td><?php echo $dataMengajar['jjm_linier']; $totJJMLinier += $dataMengajar['jjm_linier'];?></td>
                <td><?php echo $dataMengajar['nama_sekolah']; ?></td>
                <?=validate($dataMengajar['is_jjm_rombel_normal'], 'JJM_normal', 'Ya', 'Tidak')?>
                <td><a href="inforombel.php?rombel_id=<?php echo $dataMengajar['rombongan_belajar_id']; ?>&is_jjm_rombel_normal=<?=$dataMengajar['is_jjm_rombel_normal']?>&jjm_rombel=<?=$dataMengajar['jjm_rombel']?>" class="label btn-info" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=600');return false;">Info</a></td>
            </tr>
            <?php
            } # end foreach mengajar
        } # end cek nuptk mengajar
            ?>
            <?php
            if ($ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas'] > 0 ){
            ?>
            <tr>
                <td>Tugas Tambahan</td>
                <td>Mengajar Di luar Dikdas</td>
                <td><?php echo $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; $totJJM += $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; ?></td>
                <td><?php echo $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; $totJJMKTSP += $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; ?></td>
                <td><?php echo $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; $totJJMLinier += $ArrKeys['result_ptk']['jumlah_jam_mengajar_diluar_dikdas']; ?></td>
                <td colspan="3"></td>
            </tr>
            <?php
            }
            ?>
            <?php
            if ($ArrKeys['result_ptk']['jam_tugas_tambahan'] > 0 ){
            ?>
            <tr>
                <td>Tugas Tambahan</td>
                <td><?php echo $ArrKeys['result_ptk']['nama_tugas_tambahan'];?></td>
                <td><?php echo $ArrKeys['result_ptk']['jam_tugas_tambahan']; $totJJM += $ArrKeys['result_ptk']['jam_tugas_tambahan']; ?></td>
                <td><?php echo $ArrKeys['result_ptk']['jam_tugas_tambahan']; $totJJMKTSP += $ArrKeys['result_ptk']['jam_tugas_tambahan']; ?></td>
                <td><?php echo $ArrKeys['result_ptk']['jam_tugas_tambahan']; $totJJMLinier += $ArrKeys['result_ptk']['jam_tugas_tambahan']; ?></td>
                <td colspan="3"><?php echo $namaSekolah; ?></td>
            </tr>
            <?php
            }
            ?>
          <tr>
            <td colspan="2"><strong>Jumlah</strong></td>
              <td><strong><?php echo $totJJM; ?></strong></td>
              <td><strong><?php echo $totJJMKTSP; ?></strong></td>
              <td><strong><?php echo $totJJMLinier; ?></strong></td>
              <td colspan="3"></td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="clearfix"></div>
    
        <?php
        $nuptkValid = ($ArrKeys['result_ptk']['nuptk'] == $ArrKeys['result_ptk']['nuptk_link']) ? true : false;
        $nuptkValidStr = $nuptkValid ? "Valid" : "Tidak valid";
        $nuptkValidIcon = ($nuptkValid ? 'success' : 'warning');
        ?>
      <h3>Status NUPTK</h3>
      <table class="table table-striped table-condensed">
        <tbody>
          <tr>
            <td><strong>NUPTK</strong></td>
              <td width="650px"><?php echo $ArrKeys['result_ptk']['nuptk']; ?></td>
              <td width="50px"><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Nama</strong></td>
              <td><?php echo $ArrKeys['result_ptk']['nama_nuptk']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Kab/Kota</strong></td>
              <td><?php echo $ArrKeys['result_ptk']['nama_kabupaten_kota']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Nama Instansi NUPTK</strong></td>
              <td><?php echo $ArrKeys['result_ptk']['nama_instansi_nuptk']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>NUPTK Valid</strong></td>
              <td><?php $nuptkValid = ($ArrKeys['result_ptk']['nuptk'] == $ArrKeys['result_ptk']['nuptk_link']) ? "Valid" : "Tidak Valid"; echo $nuptkValid; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
    
        </tbody>
      </table>
      <div class="clearfix"></div>
    
        <!-- STATUS KELULUSAN SERTIFIKASI -->
      <h3>Status Kelulusan Sertifikasi</h3>
      <?php
      // get data tunjangan (sertifikasi)
      $redisTunjangan = getRedis(6382);
      $ArrKeys['result_tunjangan'] = $redisTunjangan->hgetall("t_tunjangan_profesi_new:".$ArrKeys['result_ptk']['nuptk_link']);
    
      // print_r($ArrKeys['result_tunjangan']);
      $ArrKeys['result_bidang_studi'] = $redisTunjangan->hgetall("t_bidang_studi:".substr($ArrKeys['result_tunjangan']['no_peserta_nrg'], 6, 3));
    
      if (($ArrKeys['result_tunjangan']))
      {
      ?>
      <table class="table table-striped table-condensed">
        <tbody>
          <tr>
            <td><strong>Atas Nama</strong></td>
              <td width="650px"><?php echo $ArrKeys['result_tunjangan']['nama']; ?></td>
              <td width="50px"><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Tahun Sertifikasi</strong></td>
              <td><?php echo "20".substr($ArrKeys['result_tunjangan']['no_peserta_nrg'], 0, 2); ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Kode Sertifikasi</strong></td>
              <td><?php echo substr($ArrKeys['result_tunjangan']['no_peserta_nrg'], 6, 3).' ('.$ArrKeys['result_bidang_studi']['nama_lama'].')'; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>No. Peserta</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['no_peserta_nrg']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>NRG</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['nrg']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Kab/Kota</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['nama_kabupaten_kota']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
        </tbody>
      </table>
      <div class="clearfix"></div>
    
        <?php
      // get data tunjangan (profesi)
      $redisTunjanganProfesi = getRedis(6384);
      $ArrKeys['result_tunjangan_profesi'] = $redisTunjanganProfesi->hgetall("t_sk_profesi_member:".$ArrKeys['result_ptk']['nuptk_link']);
        $ArrKeys['result_tunjangan_profesi']['t_sk_profesi_id'] = $ArrKeys['result_tunjangan_profesi']['t_sk_profesi_id'];
      $ArrKeys['result_tunjangan_profesi']['tgl_sk'] = $redisTunjanganProfesi->hget("t_sk_profesi:".$ArrKeys['result_tunjangan_profesi']['t_sk_profesi_id'],"tgl_sk");
      // print_r($ArrKeys['result_tunjangan_profesi']);
        ?>
        <!-- INFO TUNJANGAN PROFESI -->
      <h3>Info Tunjangan Profesi</h3>
      <table class="table table-striped table-condensed">
        <tbody>
          <tr>
            <td><strong>Status Dokumen</strong></td>
              <td width="650px"><?php
              $ArrKeys['result_tunjangan_profesi']['r_status_dokumen'] = $redisTunjanganProfesi->hgetall("r_status_dokumen:".$ArrKeys['result_tunjangan']['status_dokumen']);
              echo $ArrKeys['result_tunjangan_profesi']['r_status_dokumen']['nama']." (".$ArrKeys['result_tunjangan_profesi']['r_status_dokumen']['keterangan'].")";
              ?></td>
              <td width="50px"><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>No SK</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan_profesi']['nomor_sk']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Tanggal Terbit SK</strong></td>
              <td><?php
            $tgl = date_parse($ArrKeys['result_tunjangan_profesi']['tgl_sk']);
            $ArrKeys['result_tunjangan_profesi']['tgl_sk'] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];
              echo $ArrKeys['result_tunjangan_profesi']['tgl_sk'];
              ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Nama Bank</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['nama_bank']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Cabang Bank</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['cabang_bank']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>No Rekening</strong></td>
              <td><?php echo rekeningMasking($ArrKeys['result_tunjangan']['no_rekening']); ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Atas Nama Rekening</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['atas_nama_bank']; ?></td>
              <td><span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span></td>
          </tr>
          <tr>
            <td><strong>Keterangan</strong></td>
              <td><?php echo $ArrKeys['result_tunjangan']['keterangan_tidak_lengkap']; ?></td>
              <td><!--<span class="label btn-<?=$nuptkValidIcon?>"><?=$nuptkValidStr?></span>--></td>
          </tr>
        </tbody>
      </table>
      <div class="clearfix"></div>
      <?php
      }
      else
      {
        ?>
            <blockquote>
              <p class="text-danger">Data Kelulusan tidak ditemukan. Jika ybs telah sertifikasi pastikan bahwa NUPTK pada data kelulusan dan dapodik valid</p>
            </blockquote>
      <?php
      }
        ?>
    
      <!-- PENGUMUMAN -->
        <?php
        // cek kalo NUPTK tsb memenuhi syarat tunjangan profesi
        if ($ArrKeys['result_ptk']['memenuhi_syarat_tunjangan_profesi'] == 1){
        ?>
        <blockquote>
          <p class="text-success">* PTK bersangkutan telah <strong><u>memenuhi syarat</u></strong> untuk menerima Tunjangan Profesi</p>
        </blockquote>
        <?php
        }
        ?>
          <!--
          <p class="text-warning">* Mohon pastikan alamat e-mail pada Data Dikdas terisi dengan benar, karena link/tautan untuk konfirmasi data akan dikirim melalui e-mail.</p>
          <p class="text-warning">* Perbaiki data guru jika diperlukan melalui Dapodik Dikdas. Data akan terupdate dalam beberapa hari.</p>
          <p class="text-error">* HARAP DIPAHAMI BAHWA MEKANISME UPDATE DATA HANYA BISA DILAKUKAN MELALUI APLIKASI DAPODIK.</p>
          <p class="text-error">* KAMI <strong><u>TIDAK AKAN MENJAWAB</u></strong> REQUEST UPDATE DATA.</p>
          -->
        <a href="<?=$_SERVER["PHP_SELF"]?>?logout=true" class="btn btn-info btn-block">Logout</a><br>
        <a href="report.php?nuptk=<?php echo $ArrKeys['result_ptk']['nuptk_link']; ?>" onClick="if(confirm('Kami HANYA akan memproses jika data tidak sesuai dengan infoguru yang lama. Laporkan?')) {window.location.replace(this.href);}; return false;" class="btn btn-warning btn-block">Laporkan jika halaman ini tidak sesuai</a><br>
    </div>
    <?php
    $redisReport = getRedis(6383);
    ?>
    <footer><center>Diakses <?php echo $redisReport->get('hit'); ?> kali sejak 10 Juni 2013 Pukul 18:00 WIB</center><br></footer>
    <script type="text/javascript" src="assets/bootstrap-3.0.0/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-3.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      // !function ($) {
        $(function(){
        $( "#alert-danger" ).click(function() {
          // $.ajaxSetup ({
         //        cache: false
         //    });
          $.ajax({
            type: "POST",
            url: "update_nuptk.php",
            // dataType: "json",
            cache : false,
            data: { "nuptk": "<?php echo $ArrKeys['result_ptk']['nuptk_link']; ?>"}
          })
          .done(function( msg ) {
            console.log(msg);
            // $("jsonresult").append(msg);
            var datas = jQuery.parseJSON(msg);
            if (datas.success === true){
              alert('update done page will reload');
              window.setTimeout('location.reload()', 1000);
              return false;
            }
          })
          .fail(function() { alert("whoops"); });
        });
        })
      // }(window.jQuery)
    </script>
    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
      _paq.push(["setCookieDomain", "*.223.27.144.201"]);
      _paq.push(["setDomains", ["*.223.27.144.201"]]);
      _paq.push(["trackPageView"]);
      _paq.push(["enableLinkTracking"]);
    
      (function() {
        var u=(("https:" == document.location.protocol) ? "https" : "http") + "://223.27.144.201:8888/piwik/";
        _paq.push(["setTrackerUrl", u+"piwik.php"]);
        _paq.push(["setSiteId", "1"]);
        var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
        g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Piwik Code -->
    </body>
    </html>
    <?php
    $redisReport->incr('hit');
    unset($ArrKeys);
    if ( (isset($_REQUEST['logout'])) && ($_REQUEST['logout'] == true)){
        // redirect user ke halaman login
        header('index.html');
    }
    ?>

###4.7 
###4.8