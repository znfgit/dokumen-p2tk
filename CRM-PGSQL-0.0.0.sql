/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     6/8/2014 12:01:09 PM                         */
/*==============================================================*/


drop table PENGGUNA;

drop table TICKET;

/*==============================================================*/
/* Table: PENGGUNA                                              */
/*==============================================================*/
create table PENGGUNA (
   PENGGUNA_ID          INT8                 not null,
   NAMA                 VARCHAR(50)          null,
   constraint PK_PENGGUNA primary key (PENGGUNA_ID)
);

/*==============================================================*/
/* Table: TICKET                                                */
/*==============================================================*/
create table TICKET (
   TICKET_ID            INT8                 not null,
   PENGGUNA_ID          INT8                 not null,
   TITLE                VARCHAR(100)         null,
   DESCRIPTION          VARCHAR(200)         null,
   constraint PK_TICKET primary key (TICKET_ID)
);

alter table TICKET
   add constraint FK_TICKET_PENGGUNA__PENGGUNA foreign key (PENGGUNA_ID)
      references PENGGUNA (PENGGUNA_ID)
      on delete restrict on update restrict;

